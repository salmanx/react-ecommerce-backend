const Category = require("../controllers/category");
const Department = require("../controllers/department");
const Product = require("../controllers/product");
const Auth = require("../controllers/auth");
const Customer = require("../controllers/customer");
const Payment = require("../controllers/payment");
const Order = require("../controllers/order");

const checkAuhenticated = require("../middleware/auth.middleware");

module.exports = app => {
  app.get("/api/categories", Category.index);
  app.get("/api/categories/:id", Category.show);
  app.get("/api/departments", Department.index);
  app.get("/api/departments/:id", Department.show);
  app.get("/api/products", Product.index);
  app.get("/api/products/:id", Product.show);
  app.get("/api/search", Product.search);
  app.post("/api/signup", Auth.signup);
  app.post("/api/signin", Auth.signin);
  app.get("/api/customer/:id", checkAuhenticated, Customer.show);
  app.put("/api/customer/:id", checkAuhenticated, Customer.update);
  app.get('/api/payment', Payment.index);
  app.post('/api/payment', Payment.payment);
  app.post('/api/order', checkAuhenticated, Order.create);
};
