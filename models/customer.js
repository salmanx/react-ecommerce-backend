// interface for customer model
const Customer = customer => {
  this.customer_id = customer.customer_id;
  this.name = customer.name;
  (this.email = customer.email), (this.password = customer.password);
};

Customer.find_by_email = function find_by_email(email, result) {
  db.query(
    "Select * from customer where email = ? LIMIT 1",
    email,
    (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res[0]);
      }
    }
  );
};

Customer.find = function find(id, result) {
  db.query(
    "Select * from customer where customer_id = ? LIMIT 1",
    id,
    (err, res) => {
      if (err) {
        result(err, null);
      } else {
        result(null, res[0]);
      }
    }
  );
};

Customer.create = function create(customer, result) {
  db.query("INSERT INTO customer set ?", customer, function(err, res) {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Customer.update = function update(customer_id, customer, result) {
  db.query(
    "UPDATE customer SET name = ?, password = ?, credit_card = ?, postal_code = ?, address_1 = ?, address_2 = ?, city = ?, region = ?, country = ?, day_phone = ?, mob_phone = ? where customer_id = ?",
    [
      customer.name,
      customer.password,
      customer.credit_card,
      customer.postal_code,
      customer.address_1,
      customer.address_2,
      customer.city,
      customer.region,
      customer.country,
      customer.day_phone,
      customer.mob_phone,
      customer_id
    ],
    function(err, res) {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        console.log(res);
        result(null, res);
      }
    }
  );
};

module.exports = Customer;
