// interface for Department model
const Department = depertment => {
  this.department_id = depertment.department_id;
  this.name = depertment.name;
  this.description = Department.description;
};

Department.all = function all(result) {
  db.query("Select * from department", function(err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Department.find_products = function find_products(req, result) {
  let page = req.query.page
    ? parseInt(req.query.page) == 1
      ? 0
      : parseInt(req.query.page)
    : 0;
  let perPage = req.query.per_page ? parseInt(req.query.per_page) : 12;
  let offSet = page < 2 ? 0 : (page - 1) * 10;

  db.query(
    "SELECT  product.* FROM product INNER JOIN product_category ON product_category.product_id = product.product_id INNER JOIN category ON category.category_id = product_category.category_id WHERE category.department_id = " +
      parseInt(req.params.id) +
      " limit " +
      perPage +
      " offset " +
      offSet,
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

Department.count = function count(id, result) {
  db.query(
    "SELECT  product.* FROM product INNER JOIN product_category ON product_category.product_id = product.product_id INNER JOIN category ON category.category_id = product_category.category_id WHERE category.department_id = ?",
    parseInt(id),
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
      } else {
        result(null, res.length);
      }
    }
  );
};

module.exports = Department;
