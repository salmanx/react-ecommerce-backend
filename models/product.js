// interface for product model
const Product = product => {
  this.product_id = product.product_id;
  this.name = product.name;
  this.description = product.description;
  this.price = product.price;
  this.discounted_price = product.discounted_price;
  this.image = product.image;
  this.image_2 = product.image_2;
  this.thumbnail = product.thumbnail;
  this.display = product.display;
};

Product.all = function all(req, result) {
  let page = req.query.page
    ? parseInt(req.query.page) == 1
      ? 0
      : parseInt(req.query.page)
    : 0;
  let perPage = req.query.per_page ? parseInt(req.query.per_page) : 8;
  let offSet = page < 2 ? 0 : (page - 1) * 10;
  const sql = "SELECT * FROM product LIMIT " + perPage + " OFFSET " + offSet;
  db.query(sql, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Product.find = function find(id, result) {
  const sql = "SELECT * FROM product WHERE product_id = ? LIMIT 1";
  db.query(sql, id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Product.search = function search(keyword, result) {
  const product = "%" + keyword.trim() + "%";
  const sql = "SELECT * FROM product WHERE name like ? LIMIT 1";
  db.query(sql, product, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Product.count = function count(result) {
  const sql = "SELECT * FROM product";
  db.query(sql, function(err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res.length);
    }
  });
};

module.exports = Product;
