// interface for category model
const Category = category => {
  this.category_id = category.category_id;
  this.department_id = category.department_id;
  this.name = category.name;
  this.description = category.description;
};

Category.all = function all(result) {
  const sql = "SELECT * FROM category";
  db.query(sql, function(err, res) {
    if (err) {
      console.log("error: ", err);
      result(null, err);
    } else {
      result(null, res);
    }
  });
};

Category.find = function find(id, result) {
  const sql = "SELECT * FROM category WHERE category_id = ? LIMIT 1";
  db.query(sql, id, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Category.find_products = function find_products(req, result) {
  let page = req.query.page
    ? parseInt(req.query.page) == 1
      ? 0
      : parseInt(req.query.page)
    : 0;
  let perPage = req.query.per_page ? parseInt(req.query.per_page) : 12;
  let offSet = page < 2 ? 0 : (page - 1) * 10;
  const sql =
    "SELECT p.* FROM product_category pc INNER JOIN product p ON p.product_id = pc.product_id WHERE pc.category_id = " +
    parseInt(req.params.id) +
    " LIMIT " +
    perPage +
    " OFFSET " +
    offSet;
  db.query(sql, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res);
    }
  });
};

Category.count = function count(id, result) {
  const sql =
    "SELECT p.* FROM product_category pc INNER JOIN product p ON p.product_id = pc.product_id WHERE pc.category_id = ?";
  db.query(sql, parseInt(id), (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
    } else {
      result(null, res.length);
    }
  });
};

module.exports = Category;
