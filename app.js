const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const mysql = require("mysql");
const cors = require("cors");
const config = require("config");

// mysql configurations
const db = mysql.createConnection({
  host: config.get("dbConfig.host"),
  user: config.get("dbConfig.user"),
  password: config.get("dbConfig.password"),
  database: config.get("dbConfig.database")
});
// connect to mysql
db.connect(err => {
  if (err) throw err;
  console.log("Connected to databse!");
});
global.db = db;

const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

const routes = require("./routes/routes");
routes(app);

app.listen(5000, () => {
  console.log("App is listening 5000.");
});
