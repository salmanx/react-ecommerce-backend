const Department = require("../models/department");

module.exports = {
  // show all departments
  index: (req, res) => {
    Department.all((err, departments) => {
      if (err) return res.send(err);
      return res.status(200).send(departments);
    });
  },

  // show a Department with all its products
  show: (req, res) => {
    Department.find_products(req, (err, products) => {
      if (err) return res.send(err);
      Department.count(req.params.id, (err, productCounts) => {
        if (err) return res.send(err);
        return res.status(200).send({
          products: products,
          count: productCounts
        });
      });
    });
  }
};
