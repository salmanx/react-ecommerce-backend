const bcrypt = require("bcryptjs");
const Customer = require("../models/customer");

module.exports = {
  // show a customer with all its attributes
  show: (req, res) => {
    Customer.find(req.params.id, (err, customer) => {
      if (err) return res.send(err);
      return res.status(200).json(customer);
    });
  },

  update: (req, res) => {
    Customer.find(req.user.customer_id, (err, customer) => {
      if (err) return res.status(400).send(err);
      if (customer) {
        const saltRounds = 10;
        bcrypt.genSalt(saltRounds, function(err, salt) {
          bcrypt.hash(req.body.password, salt, function(err, hash) {
            if (err) return res.send(err);
            req.body.password = hash;
            Customer.update(req.user.customer_id, req.body, (err, result) => {
              if (err) return res.send(err);
              console.log(result);
              res.status(200).json(result);
            });
          });
        });
      } else {
        res.status(400).send("Ops coustomer not found!");
      }
    });
  }
};
