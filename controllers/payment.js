const config = require("config");
const configureStripe = require("stripe");
const stripe = configureStripe(config.get("stripeKey"));

const postStripeCharge = res => (stripeErr, stripeRes) => {
  if (stripeErr) {
    console.log(stripeErr);
    res.status(500).send({ error: stripeErr });
  } else {
    res.status(200).send({ success: stripeRes });
  }
};

module.exports = {
  index: (req, res) => {
    res.send({
      message: "Hello Stripe checkout server!",
      timestamp: new Date().toISOString()
    });
  },

  payment: (req, res) => {
    stripe.charges.create(req.body, postStripeCharge(res));
  }
};
