const bcrypt = require("bcryptjs");
const _ = require("lodash");
const jwt = require("jsonwebtoken");
const config = require("config");
const authGuard = require("../middleware/auth.middleware");
const Customer = require("../models/customer");

module.exports = {
  signin: (req, res) => {
    if (!req.body.email) {
      return res.status(400).send("Please provide email");
    }

    Customer.find_by_email(req.body.email, (err, foundCustomer) => {
      if (err) return res.send(err);

      if (foundCustomer) {
        bcrypt.compare(req.body.password, foundCustomer.password, function(
          err,
          result
        ) {
          if (err) return res.send("err");
          if (result) {
            //  sign a token with 1 hour expiration
            const token = jwt.sign(
              {
                customer_id: foundCustomer.customer_id,
                name: foundCustomer.name,
                email: foundCustomer.email,
                exp: Math.floor(Date.now() / 1000) + 60 * 60
              },
              config.get("jwtPrivateKey")
            );
            return res.status(200).json({ token });
          } else {
            return res.status(400).send("Email/password did not match!");
          }
        });
      } else {
        return res
          .status(400)
          .send("Sorry we dont find any match from our records");
      }
    });
  },

  signup: (req, res) => {
    if (!req.body.email || !req.body.name || !req.body.password) {
      return res.status(400).send("Please provide customer details");
    }

    Customer.find_by_email(req.body.email, (err, foundCustomer) => {
      if (err) return res.send(err);

      if (!foundCustomer) {
        const saltRounds = 10;
        bcrypt.genSalt(saltRounds, function(err, salt) {
          bcrypt.hash(req.body.password, salt, function(err, hash) {
            if (err) return res.send(err);
            // Store hash in DB.
            Customer.create(
              { name: req.body.name, email: req.body.email, password: hash },
              function(err, result) {
                if (err) return res.send(err);
                //  sign a token with 1 hour expiration
                const token = jwt.sign(
                  {
                    customer_id: result.insertId,
                    name: req.body.name,
                    email: req.body.email,
                    exp: Math.floor(Date.now() / 1000) + 60 * 60
                  },
                  config.get("jwtPrivateKey")
                );
                console.log(token);
                return res.status(200).json({ token });
              }
            );
          });
        });
      } else {
        return res
          .status(400)
          .send("This email is already taken. Plase try with another one.");
      }
    });
  }
};
