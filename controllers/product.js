const Product = require("../models/product");

module.exports = {
  // show all products
  index: (req, res) => {
    Product.all(req, (err, products) => {
      if (err) return res.send(err);
      Product.count((err, productCounts) => {
        if (err) return res.send(err);

        return res.status(200).send({
          products: products,
          count: productCounts
        });
      });
    });
  },

  // show a single product
  show: (req, res) => {
    Product.find(req.params.id, (err, product) => {
      if (err) return res.send(err);
      return res.status(200).send(product[0]);
    });
  },

  search: (req, res) => {
    if (req.query.keyword !== undefined || req.query.keyword !== null) {
      Product.search(req.query.keyword, (err, product) => {
        if (err) return res.send(err);
        return res.status(200).send(product);
      });
    }
  }
};
