const nodeMailer = require("nodemailer");

module.exports = {
  create: (req, res) => {
    nodeMailer.createTestAccount((err, account) => {
      // create reusable transporter object using the default SMTP transport
      let transporter = nodeMailer.createTransport({
        host: "smtp.ethereal.email",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: account.user, // generated ethereal user
          pass: account.pass // generated ethereal password
        }
      });

      let mailOptions = {
        from: '"Salman Mahmud"',
        to: req.user.email,
        subject: "Confirmed your order",
        text: JSON.stringify(req.body),
        html: `<b><pre>${JSON.stringify(req.body)}</pre></b>`
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.log(error);
          res.status(400).send({ success: false });
        } else {
          res.status(200).send({ success: true });
        }
      });
    });
  }
};
