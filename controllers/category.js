const Category = require("../models/category");

module.exports = {
  // show all categories
  index: (req, res) => {
    Category.all((err, categories) => {
      if (err) return res.send(err);
      return res.status(200).send(categories);
    });
  },

  // show a category with all its products
  show: (req, res) => {
    Category.find_products(req, (err, products) => {
      if (err) return res.send(err);
      Category.count(req.params.id, (err, productCounts) => {
        if (err) return res.send(err);
        return res.status(200).send({
          products: products,
          count: productCounts
        });
      });
    });
  }
};
